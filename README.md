# Diff Revision Activity Metrics

## If This Helps You, Please Star This Project :)

One click can help us keep providing and improving Guided Explorations.  If you find this information helpful, please click the star on this project's details page! [Project Details](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/git-diff-revision-activity-metrics)

## Overview Information

GitLab's features are constantly and rapidly evolving and we cannot keep every example up to date.  The date and version information are published here so that you can assess if new features mean that the example could be enhanced or does not account for an new capability of GitLab.

- **Product Manager For This Guided Exploration**: Darwin Sanoy (@DarwinJS)

- **Publish Date**: 2021-08-09

- **GitLab Version Released On**: 14.0

- **GitLab Edition Required**: 

  - For overall solution: [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/) 

    [Click to see Features by Edition](https://about.gitlab.com/features/) 

- **Tested On**: 
  - GitLab Docker-Executor Runner

- **References and Featured In**:

  - Video: [Git Diff Revision Activity Metrics Walkthrough](https://youtu.be/ti1vBgBbMRM)
  - An example of calling this CI extension in [Diff Revision Activity Analytics Demo](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/calling-extensions-examples/diff-revision-activity-analytics-demo)
  - Another Guided Exploration that uses SCC to count ALL your code [CI CD Plugin Extension SCC Code Counter](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/ci-cd-plugin-extension-scc)

## Demonstrates These Design Requirements, Desirements and AntiPatterns

- **GitLab CI Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Implemented as a CI Extension so that it is easily included in any project.  This includes per-project overrides of the threshold for level labels.
- **GitLab CI Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** [GitLab DOTENV Artifacts](https://docs.gitlab.com/ee/ci/variables/#pass-an-environment-variable-to-another-job) for propagating variables into the pipeline.
- **GitLab CI Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** [GitLab expose_as: keyword](https://docs.gitlab.com/ee/ci/yaml/#artifactsexpose_as) for making artifacts easily accessible from an MR.
- **GitLab CI Development Pattern [![FC](https://gitlab.com/guided-explorations/guided-exploration-concept/-/raw/master/images/FC.png)](https://about.gitlab.com/features/):** Uses [GitLab Merge Request API](https://docs.gitlab.com/ee/api/merge_requests.html) to update labels on MRs.

## Feature List

> ATTENTION: This is an experiment. The default algorithms of this solution are untuned and unproven to create an accurately predictive modelling of MR Review Time Effort. It is also unproven whether the data set being analyzed can even create a predictive model that any given organization finds useful.  Additionally, even if the model was proven by running many experiments, it is likely to need tuning by every organization and possibly each development team. The solution does allow for this tuning while still functioning as a depended upon extension using includes.

- Attempts to give a measure of the level of revision activity that has occured between two branches, both as a number and as a level to give an impression of how challenging it may be for human reviewers to properly qualify the changes.
- Configuration (set these in the projects using this extension):
  - **DIFFREVLEVEL_MRLABELLING**: Optional, defaults to disabled. Change to 'true' to passively attempt to update MR LABELs.  You must also be providing MR_UPDATE_TOKEN somewhere in the pipeline.
  - **REVISION_MULTIPLIER_FACTOR**: Optional, Overrides default of 1000. The multiplier applied to the get the REVISION_FACTOR via this formula REVISION_FACTOR = (git added lines + git removed lines)/file totals lines) * REVISION_MULTIPLIER_FACTOR
  - **REVISION_FACTOR_TO_MINS_CONVERSION_PERCENT**: Optional: Setting this both enables time estimates and provides the factor to use when creating them. Multiplied against REVISION_FACTOR to come up with a time estimate in minutes via this formula: REVISION_REVIEW_EST_MINS = REVISION_FACTOR * REVISION_FACTOR_TO_MINS_CONVERSION_PERCENT 
  - **THRESHOLD_LOW**: Optional, Overrides default of 0.
  - **THRESHOLD_MEDIUM**: Optional, Overrides default of 250.
  - **THRESHOLD_HIGH**: Optional, Overrides default of 750.
  - **THRESHOLD_EXTREME**: Optional, Overrides default of 1000.

- Creates Metrics:
  - **REVISION_FACTOR**: an arbitrary number that attempts to assess amount of revision. Purposely obfuscated from actual values like agile planning sizes.  Any choosen algorithm is likely to generate fairly jittery data.  Algorithm should be tuned by observing results and adjusting. 
    - **Default Algorithm**: (Git Adds + Git Removes) / SCC Total File Lines \* 1000 rounded to integer.
    > Note this algorithm does not account for the actual code getting simpler as it only measures revision levels.  If an individual changes code 100 times and in the end it was much simpler in execution and human comprehension, this algorithm will still judge the high level of revision to generate a high level of review effort.
  - **REVISION_LEVEL**: A determination sueh as NONE, LOW, MEDIUM, HIGH, EXTREME based on whether REVISION_FACTOR exceeds configurable thresholds
  - **REVISION_REVIEW_EST_MINS**: - when REVISION_FACTOR_TO_MINS_CONVERSION_PERCENT is set (both enables and configures), it will be used as a percent to convert REVISION_FACTOR into estimated review minutes. REVISION_FACTOR_TO_MINS_CONVERSION_PERCENT should be tuned by observing results and adjusting. Note that it overwrites the estimate every time - which should be OK if they aren't also under manual management because if the MR Revision level changes, so should the time estimate. 
    - **Default Algorithm**: REVISION_FACTOR \* REVISION_FACTOR_TO_MINS_CONVERSION_PERCENT  / 100 and rounded to the nearest whole number.
- Generates HTML artifact that combines [SCC](https://github.com/boyter/scc) information, Git information and a calculated revision level.
- Also generates a text artifact right in the CI log for easy of use
- Exposes artifact on the merge request
- Optionally Labels the MR with a level label
- Optionally sets the MR time estimate
- Thresholds for level labels can be overridden with CI variables: THRESHOLD_LOW, THRESHOLD_MEDIUM, THRESHOLD_HIGH, THRESHOLD_EXTREME
- Works for MRs and branches
- REVISION_FACTOR and REVISION_LEVEL are passed up to the pipeline and to all subsequent jobs (using dotenv artifact)
- Very fast because scc is very, very fast ([Using scc to process 40 TB of files from Github/Bitbucket/Gitlab](https://boyter.org/posts/an-informal-survey-of-10-million-github-bitbucket-gitlab-projects/))

## Limitations

- Assumes that the two branches have a common ancestorial commit - from which it can generate the diff that should look very similar to that which is done during a GitLab MR.
- Has not been tested in complex branching scenarios. (Contributions of Issuse areound complex scenarios can be added)
- Does not work for merging between forked projects (Contributions Are Welcome!)

## Iterations

- The code has a commented example MR API call to update the time estimate on the MR.  This could be used instead of revision factor since it's total will rollup into higher levels - unlike the custom label whose numeric value cannot be mathematically rolled up.

## Metrics

- SCCs Complexity measurement approach is discussed in https://github.com/boyter/scc
- Diff Revision Activity Factor - is a simple algorithm in this script that defaults to (Adds + Removes) / Total File Lines * 1000 rounded to integer. 
- scc is outputing some other factors that could be used - such as "Total Code Lines" - however Git is tracking all adds and removes regardless of code or comment. When refining this calculation be careful of the algorithmic error known as "Over fitting".
- When Total File Lines equals either Git Added or Git Removed Lines then the file is completely new or was deleted.
- Git Line Changes are based on file history so can be higher than the file total lines if a lot of changes are made while refining the content.

## Screenshots

### MR List View with Tags

![mr-list-with-labels](images/mr-list-with-labels.png)

### Easy Access From MRs using `expose_as:`

![](images/easy-mr-access-with-expose-as.png)

### MR Diff Activity Report Artifact

![](images/mr-diff-report.png)

### MR Diff Activity Report In CI Logs

![](images/mr-report-in-ci-log.png)

### Diff Factor and Level Available in Subsequent Pipeline and Jobs

![](images/diff-factor-and-level-in-pipeline-vars.png)

## Iteration Heritage
This project is an iteration on or benefitted from the following previous work:
- Learning done while writing this blog article: [How much code do I have? A DevSecOps story](https://acloudguru.com/blog/engineering/how-much-code-do-i-have-a-devsecops-story)
- Learning and code from this Guided Exploration: [CI CD Extension for SCC Code Counter](https://gitlab.com/guided-explorations/ci-cd-plugin-extensions/ci-cd-plugin-extension-scc)

## Collaborators

- [Tim Poffenbarger](@tpoffenbarger), [Jefferson Jones](@@jeffersonj) - help with bash and AWK.

## Guided Explorations Concept

This Guided Exploration is built according to a specific vision and requirements that maximize its value to both GitLab and GitLab's customers.  You can read more here: [The Guided Explorations Concept](https://gitlab.com/guided-explorations/guided-exploration-concept/blob/master/README.md)

## Working Design Pattern

As originally built, this design pattern works and can be tested. In the case of plugin extensions like this one, the working pattern may be it's use in another Guided Exploration.
